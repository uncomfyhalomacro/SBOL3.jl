# SBOL.jl

[![Stable](https://img.shields.io/badge/docs-stable-blue.svg)](https://uncomfyhalomacro.codeberg.page/SBOL.jl/stable/)
[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://uncomfyhalomacro.codeberg.page/SBOL.jl/dev/)
[![Code Style: SciML](https://img.shields.io/static/v1?label=code%20style&message=SciML&color=9558b2&labelColor=389826)](https://github.com/SciML/SciMLStyle)
[![CI Status](https://ci.codeberg.org/api/badges/uncomfyhalomacro/SBOL.jl/status.svg)](https://ci.codeberg.org/uncomfyhalomacro/SBOL.jl)

---

[Synthetic Biology Open Language](https://sbolstandard.org/) implementation in [Julia](https://julialang.org)

Supports SBOL 1.x, SBOL 2.x and SBOL 3.x.

**Currently a work in progress**
